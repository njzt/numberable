/**
 * Numberable jQuery plugin.
 *
 * Extends input text to work with numbers:
 * - allows user to type only numbers
 * - adds thousands separator
 *
 * Instead of modyfying original input it creates clone which shows formatted value.
 * Value in original input is updated after each user interaction.
 *
 * TODO
 * - trim leading&trailing zeros
 * - handle up/down keys to increment/decrement value
 * - options.cloneEvent to trigger events attached to original input
 * - find some ie6 to test it :)
 */
(function($){

    var defaultOptions = {
        thousandSep: ',', // string to separate thousands
        showBase: false // do not hide base input, raw value may be seen
    };

    $.fn.numberable = function(optionsIn) {
        var options = $.extend({}, defaultOptions, optionsIn)
        this.each(function(i, _el){
            _numberable(_el, options);
        });
    }

    /**
     * Adds numberable functionality to input.
     * @param el {Element} input element
     * @param options {object} see defaultOptions
     */
    function _numberable(el, options){
        // $elRaw and valueRaw refers to original input
        // $elStr and valueStr refers to cloned input with formatted value
        var $elRaw = $(el),
            $elStr = $elRaw.clone(true),
            inputStr = $elStr.get(0),
            valueRaw,
            valueStr;
            
            $elRaw.before($elStr);
            $elStr.removeAttr('name') // do not send value when placed in form
                .removeAttr('id');  // base id stays unique

            $elStr.on({
                keydown: handleKeyDown,
                keyup: handleText,
                paste: function(){ 
                    setTimeout( function(){ handleText(true) } ,0)  // handle text when paste is completed
                }
            })

            if (options.showBase) {
                $elRaw.attr('disabled','disabled');
            } else {
                $elRaw.css('display','none');
            }

        /**
         * Handles keydown to prevent from typing unwanted characters.
         * @param e {jQuery.Event} jQuery.Event of keydown
         */
        function handleKeyDown(e) {
            var c = e.keyCode;

            if (!(
                //allowed keystrokes are:
                (c > 47 && c < 58 && !e.shiftKey) // 0-9 without shiftKey
                || (c > 95 && c < 106) // 0-9 on numpad
                || (c == 190 && this.value.indexOf('.') === -1) // . only once
                || (c == 189 && this.selectionStart === 0) // - only if first
                || c == 8 || c == 9 || c == 13 || c == 27 || c == 46 // delete, tab, enter, tab, backspace
                || (c > 34 && c < 41) // end, home, left, up, right, down
                || (e.ctrlKey && (c == 65 || c == 67 || c == 86 || c == 88)) // ctrl+A ctrl+C ctrl+V ctrl+X
            )) {
                e.preventDefault();
            }
        }

        /**
         * Handles changes in input field. Updates values in both inputs.
         * @param isPasted {boolean|jQuery.Event} true for paste, otherwise jQuery.Event for keyup
         */
        function handleText(isPasted) {
            var valueStrNew = $elStr.val();
            if (valueStrNew === valueStr) return; // do nothing if value was not changed

            var cursorPos = inputStr.selectionStart;

            valueRaw = toRaw(valueStrNew, isPasted === true);
            valueStr = toStr(valueRaw);

            $elRaw.val(valueRaw);
            $elStr.val(valueStr);

            // after setting value cursor is moved to the end, restore previous position
            var newCursorPos = cursorPos + (valueStr.length - valueStrNew.length);
            inputStr.setSelectionRange(newCursorPos, newCursorPos);
        }

        /**
         * Converts value typed by user to raw number value.
         * @param value {string} Value typed by user
         * @param isPasted {boolean} True if value came from pasting 
         * @return {string} Raw number value
         */
        function toRaw(value, isPasted) {
            value = value.replace(/[^\d.-]/g,'')
            if (isPasted) {
                // Basically converting valueStr to raw should be done by skipping thousands separator only.
                // But unwanted characters may be added by paste.
                value = value.replace(/(.)-*/g,"$1") // leave '-' only at the beginning
                    .replace(/\.(?=\d*\.)/g,"") // leave one '.' TODO remove only pasted dots
            }
            return value;
        }

        /**
         * Converts raw value to formatted value
         * @param value {string} Raw number value 
         * @return {string} formatted number value
         */
        function toStr(value) {
            var intToFormat = value.match(/^-?(\d{4,})/);
            if (intToFormat) {
                var intIn = intToFormat[1],
                    intOut = [];

                // can you beat the slice? http://jsperf.com/comma-for-thousands
                while(intIn.length) {
                    intOut.unshift(intIn.slice(-3));
                    intIn = intIn.slice(0,-3);
                }
                value = value.replace(intToFormat[1], intOut.join(options.thousandSep));
            }
            return value;
        }

    }

})(jQuery)
